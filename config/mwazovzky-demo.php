<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Example Config Fole
    |--------------------------------------------------------------------------
    |
    | Place here yopur package configuration params.
    |
    */

    'dummies_per_page' => env('DUMMIES_PER_PAGE', 20),
];
