<?php

namespace MWazovzky\Demo\Models;

use Illuminate\Database\Eloquent\Model;

class Dummy extends Model
{
    protected $fillable = ['name'];
}
