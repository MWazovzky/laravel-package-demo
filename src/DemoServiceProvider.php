<?php

namespace MWazovzky\Demo;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class DemoServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap application services.
     *
     * @return void
     */
    public function boot()
    {
        // Load package migrations.
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');

        // Publish package migrations.
        $this->publishes([
            __DIR__.'/../database/migrations/' => database_path('migrations')
        ], 'migrations');

        // Load package factories
        $this->app->make(Factory::class)->load(__DIR__ . '/../database/factories');

        // Load package api routes.
        $this->loadRoutesFrom(__DIR__ . '/../routes/api.php');

        // Load package web routes.
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');

        // Load package views.
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'mwazovzky-demo');

        // Publish package views.
        $this->publishes([
            __DIR__.'/../resources/views' => resource_path('views/vendor/mwazovzky_demo'),
        ], 'views');

        // Publish package config file.
        $this->publishes([
            __DIR__.'/../config/mwazovzky-demo.php' => config_path('mwazovzky-demo.php'),
        ], 'config');

        // Publish package vue components.
        $this->publishes([
            __DIR__.'/../resources/js/components' => base_path('resources/js/components/mwazovzky'),
        ], 'mwazovzky-demo-components');

        // Publish package scss files.
        $this->publishes([
            __DIR__.'/../resources/scss' => base_path('resources/scss/mwazovzky'),
        ], 'mwazovzky-demo-styles');
    }

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/mwazovzky-demo.php', 'mwazovzky-demo'
        );
    }
}
