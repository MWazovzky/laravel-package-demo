<?php

namespace MWazovzky\Demo;


class Demo
{
    public function hello(string $name)
    {
        return "Hello, {$name}";
    }
}
