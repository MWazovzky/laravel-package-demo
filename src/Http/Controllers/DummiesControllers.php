<?php

namespace MWazovzky\Demo\Http\Controllers;

use MWazovzky\Demo\Models\Dummy;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class DummiesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dummies = Dummy::all();

        return view('mwazovzky-demo::dummies.index', ['dummies' => $dummies]);
    }
}
