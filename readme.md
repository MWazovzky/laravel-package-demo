# Laravel Package Demo (Template)

## Package creation
Follow package commits for a step-by-step guide to create Laravel package

## Installation
1. Make sure package repository is available, should be public or accessible via ssh key.

2. Load package into project by adding it to `composer.json`
```json
// composer.json
{
    "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/MWazovzky/laravel-package-demo"
        }
    ],
    "require": {
        "mwazovzky/laravel-package-demo": "dev-master"
    }
}
```

3. Alternatively package may be registered at [packagist](https://packagist.org) and installed via composer like a regular PHP library
```
composer require mwazovzky/laravel-package-demo
```