<?php

Route::namespace('MWazovzky\Demo\Http\Controllers')
    ->middleware(['web'])
    ->group(function () {
        Route::get('/dummies', 'DummiesController@index');
    });