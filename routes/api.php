<?php

Route::namespace('MWazovzky\Demo\Http\Controllers\Api')
    ->middleware(['api'])
    ->group(function () {
        Route::get('/api/dummies', 'DummiesController@index');
    });