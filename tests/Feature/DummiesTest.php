<?php

namespace Tests\Feature;

use MWazovzky\Demo\Models\Dummy;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;


class DummiesTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function it_can_get_dummies_index_page()
    {
        factory(Dummy::class, 3)->create();

        $response = $this->get('/dummies');

        $response->assertStatus(200)
            ->assertViewIs('mwazovzky-demo::dummies.index')
            ->assertViewHas('dummies');
    }
}
