<?php

namespace Tests\Feature\Api;

use MWazovzky\Demo\Models\Dummy;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;


class DummiesTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function it_can_get_dummies_index()
    {
        factory(Dummy::class, 3)->create();

        $response = $this->json('GET', '/api/dummies');

        $response->assertStatus(200)->assertJsonCount(3, 'data');
    }
}