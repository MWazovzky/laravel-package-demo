<?php

namespace Tests\Unit;

use Tests\TestCase;

class ConfigTest extends TestCase
{
    /**
     * @test
     */
    public function it_can_read_config_params()
    {
        $this->assertEquals(20, config('mwazovzky-demo.dummies_per_page'));
    }
}
