<?php

namespace Tests\Unit;

use MWazovzky\Demo\Demo;
use Tests\TestCase;

class DemoTest extends TestCase
{
    /**
     * @test
     */
    public function it_can_say_hello()
    {
        $demo = new Demo();

        $this->assertEquals('Hello, Mike', $demo->hello('Mike'));
    }
}