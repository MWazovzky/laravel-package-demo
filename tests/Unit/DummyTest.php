<?php

namespace Tests\Unit;

use MWazovzky\Demo\Models\Dummy;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;


class DummyTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function it_can_say_hello()
    {
        $dummy = factory(Dummy::class)->create(['name' => 'Dummy']);

        $this->assertEquals('Dummy', $dummy->name);
    }
}