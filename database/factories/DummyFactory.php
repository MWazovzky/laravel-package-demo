<?php

use Faker\Generator as Faker;

$factory->define(MWazovzky\Demo\Models\Dummy::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
    ];
});
